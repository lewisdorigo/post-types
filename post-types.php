<?php
/**
 * Plugin Name:       PostTypes
 * Plugin URI:        https://bitbucket.org/madebrave/post-types
 * Description:       A class for setting up custom post types and archives in WP.
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */


if ( !defined( 'ABSPATH' ) ) {
	die();
}

if(file_exists(__DIR__.'/vendor')) {
    include __DIR__.'/vendor/autoload.php';
}

if(!function_exists("get_field")) {
    add_action('admin_notices', function() {
    	$class = 'notice notice-error';
    	$message = __('The PostTypes plugin requires Advanced Custom Fields to be enabled.');

    	printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    });
} else {

    include __DIR__.'/lib/PostType.php';
    include __DIR__.'/lib/Fields.php';

}