<?php namespace Dorigo\PostType;

use Dorigo\PostType;
use Dorigo\Singleton\Singleton;

class Fields extends Singleton {
    protected $defaultFields = [
        [
            'key' => 'field_55cb40c5e3570',
            'label' => '',
            'name' => '',
            'type' => 'message',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'message' => 'Choose the page you want associated with different post types.',
            'esc_html' => 0,
        ],
        [
            'key' => 'field_update_post_types',
            'type' => 'text',
            'wrapper' => array (
            	'class' => 'hidden-field',
            ),
        ],
    ];

    protected function __construct() {
        add_action("init", [$this, "addArchives"]);
        add_action("acf/save_post", [$this, "saveFields"]);
    }

    public function addArchives() {
        $postTypes = PostType::getAll();

        if(!$postTypes) { return ; }

        acf_add_options_sub_page(array(
            'page_title'     => 'Post Type Archives',
            'menu_title'     => 'Post Types Archives',
            'parent'         => 'options-general.php',
    		'capability'	 => 'switch_themes',
    		'slug'           => 'drgo-post-type-archive'
        ));

        $fields = $this->defaultFields;

        foreach($postTypes as $postType) {
            $type = get_post_type_object($postType);
            $labels = get_post_type_labels($type);

            if($type && $type->has_archive === true && $type->rewrite !== false) {
                $fields[] = [
        			'key' => 'field_'.hash('adler32',$postType),
        			'label' => $type->label,
        			'name' => "page_for_{$postType}",
        			'type' => 'post_object',
        			'instructions' => 'This page will become the <a href="'.admin_url('edit.php?post_type='.$postType).'" target="_blank">'.$labels->archives.'</a> page.',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'post_type' => array (
        				0 => 'page',
        			),
        			'taxonomy' => array (
        			),
        			'allow_null' => 1,
        			'multiple' => 0,
        			'return_format' => 'id',
        			'ui' => false,
                ];
            }
        }

        acf_add_local_field_group(array (
        	'key' => 'group_55cb40bb906c1',
        	'title' => 'Post Type Pages',
        	'fields' => $fields,
        	'location' => array (
        		array (
        			array (
        				'param' => 'options_page',
        				'operator' => '==',
        				'value' => 'drgo-post-type-archive',
        			),
        		),
        	),
        	'menu_order' => 0,
        	'position' => 'normal',
        	'style' => 'seamless',
        	'label_placement' => 'left',
        	'instruction_placement' => 'field',
        	'hide_on_screen' => '',
        ));

    }

    public function saveFields($post_id) {
        if($post_id !== "options") { return; }

        foreach($_POST["acf"] as $key => $value) {

            if(substr($key, -10) === '_permalink' && $value !== get_field($key,'options')) {
                flush_rewrite_rules(false);
                return;
            }

        }
    }
}

Fields::getInstance();
