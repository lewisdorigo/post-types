<?php namespace Dorigo;

class PostType {
    private static $postTypes = [];
    private static $filters;

    private $slug;
    private $rawObject;

    public function __construct(string $slug, array $object) {
        $this->slug = $slug;
        $this->rawObject = $object;

        add_action("init", [$this, "createPostType"], 9);

        if(is_null(self::$filters)) {
            $this->addFilters();
        }
    }

    private function addFilters() {
        add_filter('display_post_states', [$this,'postStates'], 100, 2);
        add_filter('page_attributes_dropdown_pages_args', [$this,'removeFromParent'], 100, 2);
        add_filter('wp_nav_menu_objects', [$this,'navActiveStates'], 10, 2);

        add_action('pre_get_posts', [$this,'postTypeArchive'], 10, 1);
        add_action('template_redirect', [$this,'setPostPageContent'], 10, 1);

        add_action('pre_get_posts', [$this,'postTypeArchive'], 10, 1);
        add_action('template_redirect', [$this,'setPostPageContent'], 10, 1);

        // Need to update queried object in the head, to ensure that we get the page’s SEO titles.
        add_action("wp_head", [$this, "setQueriedObject"], 0);
        add_action("wp_head", [$this, "resetQueriedObject"], 99999);

        self::$filters = true;
    }

    public function createPostType() {
        if($this->rawObject["has_archive"]) {
            $archive = $this->getArchive($this->slug);
            $slug = $archive ? get_page_uri($archive) : $this->slug;

            $this->rawObject["rewrite"] = [
                "slug"  => $slug,
                "with_front" => false,
                "feeds" => false
            ];
        }

        register_post_type($this->slug, $this->rawObject);

        self::$postTypes[] = $this->slug;
    }

    public static function add(string $postType) {
        self::$postTypes[] = $postType;
    }

    public static function remove(string $postType) {
        if(array_key_exists($postType, self::$postTypes)) {
            unset(self::$postTypes[$postType]);
        }
    }

    public static function get(string $postType) {
        if(in_array($postType, self::$postTypes)) {
            return get_post_type_object($postType);
        } else {
            return null;
        }
    }

    public static function getAll() {
        return self::$postTypes;
    }

    public static function postStates(array $postStates, $post) {
        if($archive = self::isArchive($post->ID)) {
            $type = get_post_type_object($archive);
            $labels = get_post_type_labels($type);

            $postStates["{$archive}_archive"] = $labels->archives;
        }

        return $postStates;
    }

    public static function isArchive($post_id = null) {
        $post = get_post($post_id);

        if(!$post) { return false; }

        if(!is_admin() && is_post_type_archive()) {
            return $post->post_type;
        }

        foreach(self::$postTypes as $postType) {
            if($post->ID == get_field("page_for_{$postType}", "options")) {
                return $postType;
            }
        }

        return false;
    }

    public static function getArchive(string $postType = "post") {
        switch($postType) {
            case "post":
                return get_option("page_for_posts");
                break;
            default:
                return get_field("page_for_{$postType}", "options");
                break;
        }

        return null;
    }

    public static function removeFromParent(array $dropdown, $post) {
        $exclude = $dropdown["exclude_tree"];
        $exclude = !is_array($exclude) ? explode(",", $exclude) : $exclude;

        foreach(self::$postTypes as $postType) {
            if($archive = self::getArchive($postType)) {
                $exclude[] = $archive;
            }
        }

        $dropdown["exclude_tree"];
        return $dropdown;
    }

    public static function navActiveStates($items, $args) {
        global $wp_query;

        $postType = null;
        $archive = null;

        if($wp_query->is_post_type_archive() || $wp_query->is_singular()) {
            $postType = $wp_query->get("post_type");
        }

        if(!$postType) { return $items; }

        $postType = $wp_query->get("post_type");
        $archive = self::getArchive($postType);


        if(!$archive) { return $items; }

        $ancestors = get_post_ancestors($archive);
        $ancestors[] = $archive;

        array_walk($items, function(&$item, $id) use($ancestors) {
            if($item->type !== "post_type") { return true; }

            if(in_array((int) $item->object_id, $ancestors)) {
                $item->classes[] = 'current-menu-ancestor';
            }
        });

        return $items;
    }

    public static function postTypeArchive($query) {
        if(!$query->is_main_query() || !$query->get("pagename")) { return $query; }

        $page = get_page_by_path($query->get("pagename"));

        if(!$page) { return $query; }

        $archive = null;

        foreach(self::$postTypes as $postType) {
            if($page->ID == get_field("page_for_{$postType}", "options")) {
                $archive = $postType;
                break;
            }
        }

        if(!$archive) { return; }

        $query->query = [
            "post_type" => $postType,
        ];

        $query->set("pagename", "");
        $query->set("post_type", $postType);

        $query->is_page = false;
        $query->is_singular = false;
        $query->is_post_type_archive = true;
        $query->is_custom_archive = true;

        return $query;
    }

    public static function setPostPageContent() {
        global $wp_query;

        if(isset($wp_query->is_custom_archive)) {
            $wp_query->post = $wp_query->queried_object;
            $wp_query->reset_postdata();
        }
    }

    public static function setQueriedObject() {
        global $wp_query;

        if(!is_post_type_archive() || is_home()) { return; }

        $postType = self::isArchive();

        if(!self::get($postType)) { return; }

        $archive = self::getArchive($postType);

        $wp_query->old_query = clone($wp_query);
        $wp_query->old_posts = array_merge([], $wp_query->posts);

        $wp_query->queried_object = $wp_query->post = get_post($archive);
        $wp_query->posts = [$wp_query->post];

        $wp_query->is_tax = false;
        $wp_query->is_archive = false;
        $wp_query->is_singular = true;
        $wp_query->is_page = true;
        $wp_query->is_post_type_archive = false;

        $wp_query->reset_postdata();
    }

    public static function resetQueriedObject() {
        global $wp_query;

        if(!isset($wp_query->old_query)) { return; }
        $oldPosts = $wp_query->old_posts;
        $wp_query = $wp_query->old_query;

        $wp_query->posts = $oldPosts;

        $wp_query->reset_postdata();
        $wp_query->reset_query();

        unset($oldPosts, $wp_query);
    }
}
